Django==1.6.11
-e git+https://github.com/randlet/django-genericdropdown.git@616a32a32ef6102a59163cadbc3d9380667549b6#egg=django_genericdropdown-master
django-tastypie==0.9.16
-e git+https://github.com/macropin/django-registration.git@19c4cb643a95040ba22ba945cd2e3b9136a2addb#egg=django_registration-master
django-braces==1.4.0
logilab-astng>=0.24.3
logilab-common>=0.62.1
mimeparse==0.1.3
python-dateutil==2.2
python-ntlm==1.1.0
pytz==2014.7
South==1.0.1
numpy
scipy
matplotlib>=1.0
django-admin-views==0.5.0
django-listable==0.3.9
pydicom==0.9.9
